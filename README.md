<H2>Docker first steps</H2>

1. Enter the `docker-file-basics` folder 
2. run `docker build .`
3. run `docker run -p 5000:8000 <image_id>` (the image_id was printed in the last line of the build step, the first 3 characters are usually enough)
4. Go to `localhost:5000` in your browser
5. Notice the printed text
6. You can modify the text in main.py, repeat steps 2-4 and the change will be reflected there.

<H3>Docker CLI </H3>

- we can see all the images using `docker images`
- we can see all running containers using `docker ps`
- we can enter the running container using `docker exec -it <container_id> /bin/sh`
- and exit the container with the `exit` command



<h3>docker errors</h3>
Sometimes after just starting the wsl, docker cli gives the following error:

`ERROR: Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?`

it can be solved by running

`sudo service docker start`