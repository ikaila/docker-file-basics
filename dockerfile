FROM python:3

COPY . .

# RUN pip install flask
RUN pip install -r requirements.txt

ENV FLASK_APP=main
ENV FLASK_RUN_PORT=8000

# EXPOSE 8000
CMD ["flask", "run", "--host=0.0.0.0"]
